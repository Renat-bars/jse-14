package ru.tsc.almukhametov.tm.controller;

import ru.tsc.almukhametov.tm.api.controller.IProjectTaskController;
import ru.tsc.almukhametov.tm.api.service.IProjectService;
import ru.tsc.almukhametov.tm.api.service.IProjectTaskService;
import ru.tsc.almukhametov.tm.api.service.ITaskService;
import ru.tsc.almukhametov.tm.model.Project;
import ru.tsc.almukhametov.tm.model.Task;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    public void showProjects(final Project project) {
        if (project == null) return;
        System.out.println("[Find project]");
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

    @Override
    public void findAllTaskByProjectId() {
        System.out.println("[Task list by project]");
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTaskByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("No task in project");
            return;
        }
        System.out.println("Task list for project");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

    @Override
    public void bindTaskByProjectId() {
        System.out.println("[Bind task to project]");
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskId == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter project Id");
        final String projectId = TerminalUtil.nextLine();
        if (projectId == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task task = projectTaskService.bindTaskById(projectId, taskId);
        if (task == null) { System.out.println("Incorrect values"); }
    }

    @Override
    public void unbindTaskByProjectId() {
        System.out.println("[Unbind task to project]");
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskId == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter project Id");
        final String projectId = TerminalUtil.nextLine();
        if (projectId == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task task = projectTaskService.unbindTaskById(projectId, taskId);
        if (task == null) { System.out.println("Incorrect values"); }
    }

    @Override
    public void removeById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeById(id);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectTaskService.removeByIndex(index);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter Name");
        final String name = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeByName(name);
        if (project == null) System.out.println("Incorrect values");
    }

    @Override
    public void clearProjects() {
        System.out.println("[ClEAR PROJECTS]");
        projectTaskService.clearProjects();
        System.out.println("[SUCCESS CLEAR]");
    }

}
