package ru.tsc.almukhametov.tm.api.entity;

import ru.tsc.almukhametov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
