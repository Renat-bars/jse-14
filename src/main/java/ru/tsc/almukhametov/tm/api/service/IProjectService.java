package ru.tsc.almukhametov.tm.api.service;

import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void create(String name, String description);

    Project add (Project project);

    void remove(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> projectComparator);

    void clear();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateById(final String id, final String name, final String description);

    Project updateByIndex(final Integer index, final String name, final String description);

    boolean existById(String id);

    boolean existByIndex(int index);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusByName(String name, Status status);

    Project setStatus(Status status);

}
